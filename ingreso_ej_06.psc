Proceso FormatoFecha
	Definir fecha, dia, mes, anio como Entero;

	Mostrar "Ingrese una fecha valida (AAAAMMDD): ";
	Leer fecha;

	anio = trunc(fecha / 10000);
	Mostrar "El anio es ", anio;

	mes = trunc((fecha - anio * 10000) / 100);
	Mostrar "El mes es ", mes;

	dia = trunc((fecha - anio * 10000) - mes * 100);
	Mostrar "El dia es ", dia;
FinProceso
