SubProceso dias = DiasRestantes(mes, dia)
	Segun mes Hacer
		1: dias = 31 - dia + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31;
		2: dias = 28 - dia + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31;
		3: dias = 31 - dia + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31;
		4: dias = 30 - dia + 31 + 30 + 31 + 31 + 30 + 31 + 30 + 31;
		5: dias = 31 - dia + 30 + 31 + 31 + 30 + 31 + 30 + 31;
		6: dias = 30 - dia + 31 + 31 + 30 + 31 + 30 + 31;
		7: dias = 31 - dia + 31 + 30 + 31 + 30 + 31;
		8: dias = 31 - dia + 30 + 31 + 30 + 31;
		9: dias = 30 - dia + 31 + 30 + 31;
		10: dias = 31 - dia + 30 + 31;
		11: dias = 30 - dia + 31;
		12: dias = 31 - dia;
	FinSegun
FinSubProceso

SubProceso dias = DiasTranscurridos(mes, dia)
	Segun mes hacer
		1: dias = dia;
		2: dias = 31 + dia;
		3: dias = 31 + 28 + dia;
		4: dias = 31 + 28 + 31 + dia;
		5: dias = 31 + 28 + 31 + 30 + dia;
		6: dias = 31 + 28 + 31 + 30 + 31 + dia;
		7: dias = 31 + 28 + 31 + 30 + 31 + 30 + dia;
		8: dias = 31 + 28 + 31 + 30 + 31 + 30 + 31 + dia;
		9: dias = 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + dia;
		10: dias = 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + dia;
		11: dias = 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + dia;
		12: dias = 31 + 28 + 31 + 30 + 31 + 30 + 31 + 31 + 30 + 31 + 30 + dia;
	FinSegun
FinSubproceso

Proceso EsMayor
	Definir diaNacimiento, mesNacimiento, anioNacimiento como Entero;
	Definir diaActual, mesActual, anioActual como Entero;
	Definir transcurridosAnioNacimiento, transcurridosEsteAnio como Entero;
	Definir diasEn18Anios como Entero;

	diasEn18Anios = 18 * 365;

	// Fecha actual
	Mostrar "Ingrese anio actual: ";
	Leer anioActual;

	Mostrar "Ingrese mes actual: ";
	Leer mesActual;

	Mostrar "Ingrese dia: ";
	Leer diaActual;

	// Fecha nacimiento
	Mostrar "Ingrese su anio de nacimiento: ";
	Leer anioNacimiento;

	Mostrar "Ingrese su mes de nacimiento: ";
	Leer mesNacimiento;

	Mostrar "Ingrese su dia de nacimiento: ";
	Leer diaNacimiento;

	// Descarto los nacidos este anio
	Si anioNacimiento == anioActual Entonces
		Mostrar "Es menor de edad.";
	Sino
		// Descarto a los nacieron hace 17 anios
		Si anioActual - anioNacimiento < 18 Entonces
			Mostrar "Es menor de edad.";
		Sino
			// Este calculo difiere por 7 dias de los anios bisiestos
			transcurridosAnioNacimiento = DiasRestantes(mesNacimiento, diaNacimiento);
			transcurridosEsteAnio = DiasTranscurridos(mesActual, diaActual);
			diasAniosIntermedios = (anioActual - anioNacimiento - 1) * 365;
			Si ((transcurridosAnioNacimiento + transcurridosEsteAnio + diasAniosIntermedios) > diasEn18Anios) Entonces
				Mostrar "Es mayor de edad";
			Sino
				Mostrar "Es menor de edad";
			FinSi
		FinSi
	FinSi
FinProceso
