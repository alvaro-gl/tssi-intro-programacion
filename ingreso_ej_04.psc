Proceso GeometriaCirculo
	Definir radio, area, perimetro como Real;

	Mostrar "Ingrese el radio del circulo: ";
	Leer radio;

	area = pi * radio * radio;
	perimetro = 2 * pi * radio;

	Mostrar "El area del circulo es ", area ;
	Mostrar "El perimetro del circulo es ", perimetro;
FinProceso
