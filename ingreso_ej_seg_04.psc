Proceso Capicua
	Definir numero, extremoIzquierdo, medioIzquierdo, centro, medioDerecho, extremoDerecho como Entero;

	Mostrar "Ingrese un numero: ";
	Leer numero;

	extremoIzquierdo = trunc(numero / 10000);
	medioIzquierdo = trunc(numero / 1000) - extremoIzquierdo * 10;
	centro = trunc(numero / 100) - extremoIzquierdo * 100 - medioIzquierdo * 10;
	medioDerecho = trunc(numero / 10) - extremoIzquierdo * 1000 - medioIzquierdo * 100 - centro * 10;
	extremoDerecho = numero - extremoIzquierdo * 10000 - medioIzquierdo * 1000 - centro * 100 - medioDerecho * 10;

	Si (extremoIzquierdo == extremoDerecho & medioIzquierdo == medioDerecho) Entonces
		Mostrar "El numero es capicua.";
	Sino
		Mostrar "El numero no es capicua.";
	FinSi
FinProceso
