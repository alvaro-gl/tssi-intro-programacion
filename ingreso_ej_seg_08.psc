Proceso CategoriaSocio
	Definir edad como Entero;

	Mostrar "Ingrese la edad del socio: ";
	Leer edad;

	Si (edad > 25) Entonces
		Mostrar "Categoria: Mayor.";
	Sino
		Si (edad > 18) Entonces
			Mostrar "Categoria: Juvenil.";
		Sino
			Si (edad > 12) Entonces
				Mostrar "Categoria: Cadete.";
			Sino
				Mostrar "Categoria: Menor.";
			FinSi
		FinSi
	FinSi
FinProceso
