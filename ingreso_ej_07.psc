Proceso QuintaParteNumero
	Definir numeroIngresado, restoQuintaParte como Entero;
	Definir quintaParte, septimaParteDelQuinto como Real;

	Mostrar "Ingrese un numero entero: ";
	Leer numeroIngresado;

	quintaParte = numeroIngresado / 5;
	Mostrar "La quinta parte del numero es: ", quintaParte;

	restoQuintaParte = numeroIngresado % 5;
	Mostrar "El resto de dividir el numero por 5 es: ", restoQuintaParte;

	septimaParteDelQuinto = quintaParte / 7;
	Mostrar "La septima parte de la quinta parte del numero es: ", septimaParteDelQuinto;
FinProceso
